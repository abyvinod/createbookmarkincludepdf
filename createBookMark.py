###################################
# Name of the programmer: Abraham #
# Date:2017-01-20                #
###################################

## Problem
# Extract bookmarks from a pdf that has to be included as is into another pdf
# created using pdflatex

## Solution
# Use pdftk, bookmark and python 

import re
import os
import subprocess

#### Parameters (To be set by the user)
# What is the path to the to-be-included pdf relative to the parent tex file?
# createBookmark.py should be in the same folder as the parent tex file.
filename=r'./courses/detectionEstimationTheory/main'
# What is the parent level for the to-be-included pdf? 
# 0 - Part 
# 1 - Chapter 
# 2 - Section 
# 3 - Subsection
# 4 - Subsubsection
parentLevel=1
# Flag for setting a bookmark to the to-be-included pdf
#If set to True, an additional bookmark is created at the parentLevel to refer
# to the first page of the to-be-included pdf. If set to False, no bookmark is created.
createBookmarkForWholePDF=True

############################### Actual code ####################################

#### pdftk command
annotatedTxt=filename+'_PDFannot.txt'
texCommandsTxt=filename+'_TexCommands.tex'
p = subprocess.Popen('pdftk '+filename+'.pdf dump_data output ' +annotatedTxt\
        , cwd=os.getcwd())
p.wait()
### Format of bookmarks as given by pdftk
# BookmarkBegin
# BookmarkTitle: Lecture 1: August 17, 2015
# BookmarkLevel: 1
# BookmarkPageNumber: 9

#### Program parameters
# Open the file readers
inputFileReader=open(annotatedTxt,'r')
outputTexCommands=open(texCommandsTxt,'w')
# Including the pdf
outputTexCommands.write('% Include the pdf\n')
outputTexCommands.write('\includepdf[link,pages={1-},nup=1x1,scale=1]{'\
        +filename+'.pdf}'+'\n\n')
outputTexCommands.write('% Include the bookmarks\n')
if createBookmarkForWholePDF:
    title=raw_input('What is the title for the parent bookmark? ')
    outputTexCommands.write('% Parent bookmark:\n')
    outputTexCommands.write('\\bookmark[level='+str(parentLevel)+',dest={'\
            +filename+'.pdf.1}]{'+title+'}'+'\n')
    outputTexCommands.write('% Other bookmarks:\n')
for line in inputFileReader:
    # Search for the header
    if re.findall('^BookmarkBegin',line):
        # Increment the line
        line=next(inputFileReader)
        # Get the title
        title=re.findall('BookmarkTitle: (.*)$', line)[0]
        # Increment the line
        line=next(inputFileReader)
        # Get the level and add it to the parent level
        level=str(int(re.findall('BookmarkLevel: ([0-9])$', line)[0])\
                +parentLevel)
        # Increment the line
        line=next(inputFileReader)
        # Get the page number
        pageNumber=re.findall('BookmarkPageNumber: ([0-9]+)$', line)[0]
        # Create the bookmark Tex command
        outputTexCommands.write('\\bookmark[level='+level+',dest={'\
                +filename+'.pdf.'+pageNumber+'}]{'+title+'}'+'\n')

# Close the file readers
inputFileReader.close()
outputTexCommands.close()
